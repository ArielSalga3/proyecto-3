#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "estadisticas.h"

void consultarPonderacion()
{

}

void simularPostulacion()
{
	
}

void mostrarPonderacion()
{
	
}

int menu()
{
	cargarEstadisticas();
	int opcion;
    do
    {
		printf("\n1. Consultar Ponderacion Carrera");
		printf("\n2. Simular Postulacion Carrera");
		printf("\n3. Mostrar Ponderaciones Facultad");
		printf("\n4. Salir.");
		printf("\n\nIntroduzca opción: ");
		scanf("%d", &opcion);
        switch(opcion)
        {
			case 1: 
					consultarPonderacion(carreras);
					break;
			case 2:
					simularPostulacion();
					break;
			case 3:
					mostrarPonderacion();
					break;
		}
	}
   	while(opcion != 4);
}

int main()
{
	estadisticas carreras[53]; 
	menu(carreras); 
	return EXIT_SUCCESS; 
}